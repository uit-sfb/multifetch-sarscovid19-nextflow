include {downloadEmbl} from './downloadEmbl.nf'
include {extractEmblSequence} from './extractEmblSequence.nf'
include {fetchMetadata} from './fetchMetadata.nf'
include {mergeMetadata} from './mergeMetadata.nf'



workflow Multifetch {
  configCh = Channel.fromPath(params.config).collect() //collect() in order to convert to singleton channel
  releaseCh = Channel.fromPath(params.release).collect() //collect() in order to convert to singleton channel

  inputCh = Channel.fromPath(params.input)
    .splitCsv(header: true, sep: '\t', strip: true)
    .map {it.genbank_accession} //Todo: filter out "missing"

  sequencesCh = Channel.fromPath(params.sequences).collect()
  emblCh = downloadEmbl(inputCh)
  extractEmblSequence(emblCh)
  collected_ch = fetchMetadata(emblCh, configCh, releaseCh).collect()

  mergeMetadata(collected_ch)

}
