process extractEmblSequence {
    tag "$acc"
    echo true

    publishDir "${params.sequences}", mode: 'move'

    input:
      tuple val(acc), path(emblFile, stageAs: 'in/*')

    output:
       path("${acc}")

    shell:
        '''
        #!/usr/bin/env python3

        import sys
        sys.path.insert(0, "/app")
        import getSeqData
        from utils import *
        import argparse
        import os
        import re
        import shutil
        from pathlib import Path

        record = getSeqData.parseEMBLFiles("!{emblFile}")
        id = str(record.id)
        if id and not os.path.exists(os.path.join("!{params.sequences}", id)):
            seq_dir = create_subdir(id)

            getSeqData.writeSeqFasta(record, seq_dir, !{params.threshold})
            getSeqData.writeFeaturesToFile(record, seq_dir)
            getSeqData.writeCDSData(record, seq_dir)

        '''
}
