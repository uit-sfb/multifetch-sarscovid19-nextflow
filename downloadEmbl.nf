process downloadEmbl {
    errorStrategy = 'ignore'

    tag "$acc"

    input:
      val acc

    output:
      tuple val(acc), path("out/${acc}.embl")

    shell:
        '''
        #!/usr/bin/env python3

        import sys
        sys.path.insert(0, "/app")
        import getEMBLFiles

        getEMBLFiles.downloadEmbl("!{acc}", "out")
        '''
}
