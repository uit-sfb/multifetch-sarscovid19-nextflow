process fetchMetadata {
    tag "$acc"
    echo true

    input:
      tuple val(acc), path(emblFile, stageAs: 'in/*')
      path(configFile, stageAs: 'in/config.tsv')
      path(releaseFile, stageAs: 'in/*')

    output:
      path("out/${acc}.tsv")

    shell:
        '''
        #!/usr/bin/env python3

        import sys
        sys.path.insert(0, "/app")
        import getSeqData
        import utils
        from pathlib import Path
        from processOutputTable import *
        from getBioSampleData import *
        from getBioProjectData import *
        from getSRAData import *
        from getEMBLMetadata import *
        from getBioProjectData import *

        attrs = getAllAttributes("!{configFile}")
        attr_defs = getAttributeOrder("!{configFile}")
        default_values = zip(attrs, getDefaultValues("!{configFile}"))
        default_df = pd.DataFrame(dict(default_values), index=[0])
        default_df.dropna(how='all', axis=1, inplace=True)

        record = getSeqData.parseEMBLFiles("!{emblFile}")
        bioId = getBioSampleId(record)
        bio_df = pd.DataFrame()
        projId = getBioProjectId(record)
        proj_df = pd.DataFrame()
        sra_df = pd.DataFrame()
        embl_df = getMetadataFromEMBL(record)
        embl_df = updateAttributes(embl_df, attr_defs.get('EMBL'))
        if projId:
            proj_df = processBioProjectData(str(record.id), projId)
            proj_df = updateAttributes(proj_df, attr_defs.get('BioProject'))
        if bioId:
            bio_df = processBioSampleData(str(record.id), bioId)
            bio_df = updateAttributes(bio_df, attr_defs.get('BioSample'))
            sra_df = processSRAData(str(record.id), bioId)
            sra_df = updateAttributes(sra_df, attr_defs.get('SRA'))

        release_df = getMetadataFromReleaseGenome("!{releaseFile}")
        release_row_df = getRowFromReleaseGenome(str(record.id), release_df)
        release_row_df = updateAttributes(release_row_df, attr_defs.get('release_genome_meta.tsv'))
        release_row_df['release_genome:accession'] = release_row_df['release_genome:accession'].apply(getRecentVersion)

        data = mergeInOrder(attr_defs, proj_df, bio_df, embl_df, sra_df, release_row_df)
        empty_df = pd.DataFrame(columns=attrs)
        df = empty_df.append(data)
        df = df.applymap(removeTabs)
        df = addDefaultValues(df, default_df)
        Path("out").mkdir(parents=True, exist_ok=True)
        utils.writeToTSV(df, "out/!{acc}.tsv")

        '''
}
