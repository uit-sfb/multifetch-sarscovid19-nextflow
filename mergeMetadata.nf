process mergeMetadata {
    echo true

    publishDir "${params.output}", mode: 'move'

    input:
      path(files, stageAs: 'in/*')

    output:
      path("out.tsv")

    shell:
        '''
        #!/usr/bin/env python3

        import sys
        sys.path.insert(0, "/app")
        import pandas as pd
        import glob
        import utils

        df = pd.DataFrame()
        for f in glob.iglob(r'in/*.tsv'):
            tmp = pd.read_csv(f, sep='\t', engine='python')
            df = df.append(tmp)
        utils.writeToTSV(df, "out.tsv")
        '''
    }
